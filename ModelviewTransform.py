import numpy as np
from math import sqrt

from os import path

from pyglet.gl import *
from pyglet.window import key

vertexDefsINIT=[]
vertexDefs=[]
polyDefs=[]

global Ociste
global OcisteINIT
global Glediste
global GledisteINIT
global planeCoeffs
polyNormalsINIT = []
polyCenterPointsINIT = []
global polyNormals
global polyCenterPoints

doBackCull = False

width = 700
height = 700

# otvara prozor
config = pyglet.gl.Config(double_buffer=False)
window = pyglet.window.Window(width=width, height=height, caption="3d transforms", resizable=False, config=config, visible=False)
window.set_location(100, 100)

def loadOG(ogDescr):
    global Ociste
    global Glediste
    Ociste = list(map(float, ogDescr.readline().split()[1:]))
    Glediste = list(map(float, ogDescr.readline().split()[1:]))

    Ociste.append(1)
    Glediste.append(1)

    global OcisteINIT
    OcisteINIT = Ociste[:]
    global GledisteINIT
    GledisteINIT = Glediste[:]

def loadVertices(objVerticesDescr):
    global vertexDefsINIT
    global polyDefs

    for line in objVerticesDescr:

        if line.startswith("v"):
            print(line, end='')
            vertexDefsINIT.append(line.split()[1:])
        elif line.startswith("f"):
            print(line, end='')
            polyDefs.append(line.split()[1:])

    vertexDefsINIT = [list(map(float, x)) for x in vertexDefsINIT]
    polyDefs = [list(map(int, x)) for x in polyDefs]


def getViewMatrix(O, G):
    VG = np.array([G[0], G[1], G[2], 1])
    T1 = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[-O[0], -O[1], -O[2], 1]])

    G1 = VG.dot(T1)
    xG1, yG1, zG1, _ = G1

    if yG1 != 0:
        sinAlpha = yG1 / (sqrt(xG1**2 + yG1**2))
        cosAlpha = xG1 / (sqrt(xG1**2 + yG1**2))
        T2 = np.array([[cosAlpha,-sinAlpha,0,0],[sinAlpha,cosAlpha,0,0],[0,0,1,0],[0,0,0,1]])
    else:
        T2 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    G2 = G1.dot(T2)
    xG2, yG2, zG2, _ = G2

    if xG2 != 0:
        sinBeta = xG2 / (sqrt(xG2**2 + zG2**2))
        cosBeta = zG2 / (sqrt(xG2**2 + zG2**2))
        T3 = np.array([[cosBeta,0,sinBeta,0],[0,1,0,0],[-sinBeta,0,cosBeta,0],[0,0,0,1]])
    else:
        T3 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])


    T4 = np.array([[0,-1,0,0],[1,0,0,0],[0,0,1,0],[0,0,0,1]])

    T5 = np.array([[-1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])

    T = T1.dot(T2).dot(T3).dot(T4).dot(T5)

    return T

def getPerspectiveProjectionMatrix(O, G):
    H = sqrt((O[0]-G[0])**2 + (O[1]-G[1])**2 + (O[2]-G[2])**2)

    if H==0:
        print("Ociste i glediste se poklapaju!")
        return np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    P = np.array([[1,0,0,0],[0,1,0,0],[0,0,0,1/H],[0,0,0,0]])

    return P


def translateObject(vertices, dx, dy, dz):
    return [[v[0]+dx, v[1]+dy, v[2]+dz] for v in vertices]

def scaleObject(vertices, scaleFactor):
    return [[v[0]*scaleFactor, v[1]*scaleFactor, v[2]*scaleFactor] for v in vertices]



@window.event
def on_draw():
    window.clear()

    vs = scaleObject(vertexDefs, 50)
    vs = translateObject(vs, width/2, height/2, 0)

    glColor3f(1.0, 0.0, 0.0)
    glBegin(GL_POINTS)
    if doBackCull:
        for i in range(len(vs)):
            allPolygWithPointBack = True
            for j in range(len(polyDefs)):
                if i+1 in polyDefs[j] and not(polygonBack(j)):
                    allPolygWithPointBack = False
                    break
            if not allPolygWithPointBack:
                v = vs[i]
                glVertex2f(v[0], v[1])
    else:
        for v in vs:
            glVertex2f(v[0], v[1])
    glEnd()

    glColor3f(0.0, 0.0, 0.0)
    glBegin(GL_LINES)

    polyDefsLen = len(polyDefs)
    for i in range(polyDefsLen):
        if doBackCull and polygonBack(i):
            continue

        p = polyDefs[i]
        point0 = vs[p[0] - 1]
        point1 = vs[p[1] - 1]
        point2 = vs[p[2] - 1]

        glVertex2f(point0[0], point0[1])
        glVertex2f(point1[0], point1[1])

        glVertex2f(point0[0], point0[1])
        glVertex2f(point2[0], point2[1])

        glVertex2f(point1[0], point1[1])
        glVertex2f(point2[0], point2[1])

    glEnd()

    glFlush()

def polygonBack(polyNum):
    polyCenterPoint = polyCenterPoints[polyNum]

    polyCenterOcisteVect = [Ociste[0] - polyCenterPoint[0], Ociste[1] - polyCenterPoint[1],
                            Ociste[2] - polyCenterPoint[2], Ociste[3] - polyCenterPoint[3]]
    normalVect = polyNormals[polyNum]

    scalarProd = polyCenterOcisteVect[0]*normalVect[0]+polyCenterOcisteVect[1]*normalVect[1]+polyCenterOcisteVect[2]*normalVect[2]

    return scalarProd < 0


@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h
    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(3.0)
    glColor3f(0.0, 0.0, 0.0)


@window.event
def on_key_press(symbol, modifiers):
    global Ociste
    global Glediste
    if symbol == key.Q: # xO +
        Ociste[0] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.A: # xO -
        Ociste[0] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.W: # yO +
        Ociste[1] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.S: # yO -
        Ociste[1] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.E: # zO +
        Ociste[2] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.D: # zO -
        Ociste[2] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.T: # xG +
        Glediste[0] += 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key.G: # xG -
        Glediste[0] -= 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key.Z: # yG +
        Glediste[1] += 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key.H: # yG -
        Glediste[1] -= 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key.U: # zG +
        Glediste[2] += 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key.J: # zG -
        Glediste[2] -= 1
        print("Glediste = ({}, {}, {})".format(*Glediste))
        onOGChange()
    elif symbol == key._0:  # reset na pocetni O i G
        print("Reset na pocetno ociste i glediste...")
        Ociste = OcisteINIT[:]
        Glediste = GledisteINIT[:]
        global planeCoeffsINIT
        onOGChange()

    glFlush()

def getCenterRange(vs):
    xCoords = [v[0] for v in vs]
    yCoords = [v[1] for v in vs]
    zCoords = [v[2] for v in vs]

    xmin, xmax, ymin, ymax, zmin, zmax = min(xCoords), max(xCoords), min(yCoords), max(yCoords), min(zCoords), max(
        zCoords)

    xCenter = (xmin + xmax) / 2
    yCenter = (ymin + ymax) / 2
    zCenter = (zmin + zmax) / 2


    maxRange = max(xmax-xmin, ymax-ymin, zmax-zmin)

    return xCenter, yCenter, zCenter, maxRange

def onOGChange():
    if checkTestPointWithinBody(Ociste):
        print("Ociste je unutar tijela!")
    else:
        print("Ociste ",Ociste," je ok.")

    T = getViewMatrix(Ociste, Glediste)
    P = getPerspectiveProjectionMatrix(Ociste, Glediste)

    verticesO = vertexDefsINIT.dot(T)
    verticesP = verticesO.dot(P)


    global vertexDefs
    vertexDefs = []

    vertexDefsNew = []
    for x in verticesP:
        if (x[3] == 0):
            vertexDefsNew.append([x[0], x[1], x[2], 0])
            continue
        vertexDefsNew.append([x[0]/x[3],x[1]/x[3],x[2]/x[3],1])

    vertexDefs = vertexDefsNew


    if doBackCull:
        global polyCenterPoints
        polyCenterPointsNew = []
        for x in polyCenterPoints:
            if (x[3] == 0):
                polyCenterPointsNew.append([x[0], x[1], x[2], 1])
                continue
            polyCenterPointsNew.append([x[0]/x[3],x[1]/x[3],x[2]/x[3],1])
        polyCenterPoints = polyCenterPointsNew



def calcPlaneEquations(vertices):
    global planeCoeffs
    global polyNormals
    global polyCenterPoints
    planeCoeffs = []
    polyNormals = []
    polyCenterPoints = []
    for p in polyDefs:
        point1 = vertices[p[0] - 1]
        point2 = vertices[p[1] - 1]
        point3 = vertices[p[2] - 1]
        a = (point2[1]-point1[1])*(point3[2]-point1[2]) - (point2[2]-point1[2])*(point3[1]-point1[1])
        b = -(point2[0]-point1[0])*(point3[2]-point1[2]) + (point2[2]-point1[2])*(point3[0]-point1[0])
        c = (point2[0]-point1[0])*(point3[1]-point1[1]) - (point2[1]-point1[1])*(point3[0]-point1[0])
        d = -a * point1[0] - b * point1[1] - c * point1[2]
        planeCoeffs.append((a, b, c, d))

        if doBackCull:
            polyNormals.append([a, b, c])
            xC, yC, zC, _ = getCenterRange([point1, point2, point3])
            polyCenterPoints.append([xC, yC, zC])

    if doBackCull:
        polyCenterPointsHom = [[v[0], v[1], v[2], 1] for v in polyCenterPoints]
        polyCenterPointsArr = np.array(polyCenterPointsHom)
        polyCenterPoints = polyCenterPointsArr


        polyNormalsHom = [[n[0], n[1], n[2], 0] for n in polyNormals]
        polyNormalsArr = np.array(polyNormalsHom)
        polyNormals = polyNormalsArr

    return planeCoeffs

def checkTestPointWithinBody(testPnt):
    for plane in planeCoeffsINIT:
        r = plane[0] * testPnt[0] + plane[1] * testPnt[1] + plane[2] * testPnt[2] + plane[3]
        if r > 0:
            return False
    return True


def enterObjFilename():
    objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
    if len(objFilename) == 0:
        return "kocka.obj"
    objFilepath = "./" + objFilename
    while not(path.exists(objFilepath) and objFilepath.endswith(".obj")):
        print("Datoteka " + objFilename + " ne postoji.")
        objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
        if len(objFilename) == 0:
            return "kocka.obj"
        objFilepath = "./" + objFilename

    return objFilepath

def enterOGFilename():
    OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
    if len(OGFilename) == 0:
        return "initOG.txt"
    OGFilepath = "./" + OGFilename
    while not(path.exists(OGFilepath) and OGFilepath.endswith(".txt")):
        print("Datoteka " + OGFilename + " ne postoji.")
        OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
        if len(OGFilename) == 0:
            return "initOG.txt"
        OGFilepath = "./" + OGFilename

    return OGFilepath

def enterIsBackCull():
    ynBackCullStr = input("Zelite li uklanjanje straznjih poligona (prazno za default: ne) [da/ne]: ")
    if len(ynBackCullStr) == 0:
        return
    while ynBackCullStr.strip().lower() not in ("da", "ne"):
        ynBackCullStr = input("Zelite li uklanjanje straznjih poligona (prazno za default: ne) [da/ne]: ")
        if len(ynBackCullStr) == 0:
            return
    if ynBackCullStr.strip().lower() == "da":
        global doBackCull
        doBackCull = True



if __name__ == "__main__":
    OGFilepath = enterOGFilename()
    print("Ucitavanje datoteke ",OGFilepath,"...")
    with open(OGFilepath, "r") as ogDescr:
        loadOG(ogDescr)


    objFilepath = enterObjFilename()
    print("Ucitavanje datoteke ", objFilepath, "...")
    with open(objFilepath, "r") as objVerticesDescr:
        loadVertices(objVerticesDescr)

    enterIsBackCull()

    xc, yc, zc, maxRange = getCenterRange(vertexDefsINIT)

    print("Translacija sredista objekta u ishodiste...")
    vertexDefsINIT = translateObject(vertexDefsINIT, -xc, -yc, -zc)

    print("Skaliranje na [-1, 1]...")
    vertexDefsINIT = scaleObject(vertexDefsINIT, 2 / maxRange)

    print()
    print("Translatiran i skaliran objekt:")
    for v in vertexDefsINIT:
        print("v {} {} {}".format(*v))
    for p in polyDefs:
        print("f {} {} {}".format(*p))


    verticesHom = [[v[0],v[1],v[2],1] for v in vertexDefsINIT]
    verticesArr = np.array(verticesHom)
    vertexDefsINIT = verticesArr

    planeCoeffsINIT = calcPlaneEquations(vertexDefsINIT)[:]
    onOGChange()


    window.set_visible(True)

    pyglet.app.run()
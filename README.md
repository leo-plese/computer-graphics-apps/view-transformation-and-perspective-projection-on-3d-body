View Transformation and Perspective Projection on 3D body. Implemented in Python using NumPy library and pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "5. VJEZBA."

Created: 2020
